package swap

import (
	"os"
	"path/filepath"
	"time"
)

func SwapFileNames(
	oldFile string,
	newFile string) (string, error) {

	oldFIlePreserved := filepath.Base(oldFile)
	oldFIlePreserved += ".preserve"
	err := os.Rename(oldFile, oldFIlePreserved)
	if err != nil {
		return "", err
	}

	time.Sleep(1 * time.Second) // windows is slow

	err = os.Rename(newFile, oldFile)
	return oldFIlePreserved, err
}
